import React, { Component } from 'react';

import CounterControl from '../../components/CounterControl/CounterControl';
import CounterOutput from '../../components/CounterOutput/CounterOutput';

import { connect } from 'react-redux';

class Counter extends Component {
    state = {
        counter: 0
    }

    render () {
        return (
            <div>
                <CounterOutput value={this.props.ctr} />
                <CounterControl label="Increment" clicked={this.props.onIncrementCounter} />
                <CounterControl label="Decrement" clicked={this.props.onDecrementCounter}  />
                <CounterControl label="Add 5" clicked={this.props.add} />
                <CounterControl label="Subtract 5" clicked={this.props.substract}  />
                <hr/>
                <button onClick={this.props.onStoreResult}>Store Result</button>
                <ul>
                    {this.props.results.map(element => <li key={element.id} onClick={this.props.onDeleteResult}>{element.value}</li>)}
                </ul>
            </div>
        );
    }
}

const mapStateToProps = state => {
    return {
        ctr: state.counter,
        results: state.results
    };
}

const mapDispatchToProps = dispatch => {
    return {
        onIncrementCounter: () => dispatch({type:'INCREMENT'}),
        onDecrementCounter: () => dispatch({type:'DECREMENT'}),
        add: () => dispatch({type:'ADD', payload:5}),
        substract: () => dispatch({type:'SUBSTRACT', payload:5}),
        onStoreResult: () => dispatch({type:'STORE_RESULT'}),
        onDeleteResult: () => dispatch({type:'DELETE_RESULT'})
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(Counter);